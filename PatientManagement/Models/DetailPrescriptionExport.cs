﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientManagement.Models
{
    public class DetailPrescriptionExport
    { 

        public int Amount { get; set; }

        
        public string Unit { get; set; }

        
        public string Note { get; set; }

        public string MedicineName { get; set; }
    }
}
